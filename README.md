# Packer-Base Ansible Role

This role builds an Omnia framework base AMI using Packer.

"Base" includes operating system, software, and customizations that are shared across all application platforms.

## Pre-Requisites

* Packer installed on the host where this role runs.
* A separate `base` role containing all customizations.
  * This role runs the `base` role on an EC2 instance during AMI generation.
  * The `base` role typically lives within the client project repo's `roles` directory.
  * If `base` depends on other Ansible roles, include a Galaxy requirements file under `base/files/requirements.yml`. These roles will be installed in the Packer workspace.
  * To include other roles from the infrastructure repo create a `base_role_list` with the relevant directories under the `infrastructure/roles/` to include.

## Usage

Base AMI's are built on an existing AMI, typically a clean operating system AMI. The role can take a source AMI ID or a name and owner id filter to search.

To use a specific AMI:

```ansible
  - role: reactiveops.packer-base
    base_source_ami_id: 'ami-XXXXXX'
```

To search for a public AMI leave `base_source_ami_id` undefined. The following default search filter variables can be left off or overridden if needed.

```ansible
  - role: reactiveops.packer-base
    base_source_ami_search_name: 'ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*'
    base_source_ami_search_owner: '099720109477' # Canonical's AWS id
    base_source_ami_search_virt_type: 'hvm'
```

You can also specify the Ansible version by setting `base_ansible_version: '2.1.0.1'` (version shown is the default)

To use an instance profile and override the roles copied from the clients infrastructure repo:

```ansible

  - role: reactiveops.iam-role-develop
    iam_role_name: 'packer_base_role'
    policy_name: 'ro_account_secrets'

  - role: reactiveops.packer-base
    base_role_list:
      - 'base'
      - 'client.common'
      - 'client.something'
    base_iam_instance_profile: 'packer_base_role'
```
